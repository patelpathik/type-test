import express, { Request, Response, Router } from "express";
export class User {
  userRouter: Router = express.Router();

  constructor() {
    this.userRouter.get("/", this.welcome);
    this.userRouter.get("/:no", this.square);
  }

  calculate(n: number): number {
    return n * n;
  }

  async square(req: Request, res: Response) {
    console.log(req.body);
    let n: number = req.body.n;
    res.send({
      n: this.calculate(n),
    });
  }

  async welcome(req: Request, res: Response) {
    res.send({
      msg: "Welcome User"
    });
  }

}

