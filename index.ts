import express, { Express, Request, Response } from "express";

import { User } from "./user";

const app: Express = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req: Request, res: Response) => {
  res.send({ msg: "Express + TypeScript Server" });
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running at port ${PORT}`);
});

app.use("/user", new User().userRouter);